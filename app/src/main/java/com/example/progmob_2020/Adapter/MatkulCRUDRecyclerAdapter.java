package com.example.progmob_2020.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.progmob_2020.Model.Dosen;
import com.example.progmob_2020.Model.MataKuliah;
import com.example.progmob_2020.R;

import java.util.ArrayList;
import java.util.List;

public class MatkulCRUDRecyclerAdapter extends RecyclerView.Adapter<MatkulCRUDRecyclerAdapter.ViewHolder> {
    private Context context;
    private List<MataKuliah> matkulList;


    public MatkulCRUDRecyclerAdapter(Context context) {
        this.context = context;
        matkulList = new ArrayList<>();
    }

    public MatkulCRUDRecyclerAdapter(List<MataKuliah> matkulList) {
        this.matkulList = matkulList;
    }

    public List<MataKuliah> getMatkulList() {
        return matkulList;
    }

    public void setMatkulList(List<MataKuliah> matkulList) {
        this.matkulList = matkulList;
        notifyDataSetChanged();
    }
    @NonNull
    @Override
    public MatkulCRUDRecyclerAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_list_card_view_matkul,parent, false);
        return new MatkulCRUDRecyclerAdapter.ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull MatkulCRUDRecyclerAdapter.ViewHolder holder, int position) {
        MataKuliah mk = matkulList.get(position);

        holder.tvNama.setText(mk.getNama());
        holder.tvKode.setText(mk.getKode());
        holder.tvHari.setText(mk.getHari());
        holder.tvSesi.setText(mk.getSesi());
        holder.tvSks.setText(mk.getSks());
        holder.mk = mk;
    }

    @Override
    public int getItemCount() {
        return matkulList.size();
    }
    public class ViewHolder extends  RecyclerView.ViewHolder{
        private TextView tvNama, tvKode , tvHari, tvSesi, tvSks;
        private RecyclerView rvGetMatkulAll;
        MataKuliah mk;

        public ViewHolder(@NonNull View itemView){
            super(itemView);
            tvNama = itemView.findViewById(R.id.tvNama);
            tvKode = itemView.findViewById(R.id.tvKode);
            tvHari = itemView.findViewById(R.id.tvHari);
            tvSesi = itemView.findViewById(R.id.tvSesi);
            tvSks = itemView.findViewById(R.id.tvSks);
            rvGetMatkulAll = itemView.findViewById(R.id.rvGetMatkulAll);

        }
    }


}
