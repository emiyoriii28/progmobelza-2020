package com.example.progmob_2020.CRUD;

import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.progmob_2020.Model.DefaultResult;
import com.example.progmob_2020.Network.GetDataService;
import com.example.progmob_2020.Network.RetrofitClientInstance;
import com.example.progmob_2020.R;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MatkulUpdateActivity extends AppCompatActivity {
    EditText editTextKodeCari, editTextNama, editTextKode, editTextHari, editTextSesi, editTextSks;
    Button btnUbah;
    ProgressDialog pd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_matkul_update);

        editTextKodeCari = (EditText) findViewById(R.id.editTextKodeCari);
        editTextNama = (EditText) findViewById(R.id.editTextNamaBaru);
        editTextKode = (EditText) findViewById(R.id.editTextKodeBaru);
        editTextHari = (EditText) findViewById(R.id.editTextHariBaru);
        editTextSesi = (EditText) findViewById(R.id.editTextSesiBaru);
        editTextSks = (EditText) findViewById(R.id.editTextSksBaru);
        btnUbah = (Button) findViewById(R.id.buttonUbah);
        pd = new ProgressDialog(MatkulUpdateActivity.this);

        Intent data = getIntent();
        editTextKodeCari.setText(data.getStringExtra("kode"));
        editTextNama.setText(data.getStringExtra("nama"));
        editTextKode.setText(data.getStringExtra("kode"));
        editTextHari.setText(data.getStringExtra("hari"));
        editTextSesi.setText(data.getStringExtra("sesi"));
        editTextSks.setText(data.getStringExtra("sks"));

        btnUbah.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pd.setTitle("Silahkan Tunggu");
                pd.show();

                GetDataService service = RetrofitClientInstance.getRetrofitInstance().create(GetDataService.class);
                Call<DefaultResult> call = service.update_matkul(
                        editTextNama.getText().toString(),
                        editTextKode.getText().toString(),
                        editTextKodeCari.getText().toString(),
                        editTextHari.getText().toString(),
                        editTextSesi.getText().toString(),
                        editTextSks.getText().toString(),
                        "Fotonya Diubah"
                );

                call.enqueue(new Callback<DefaultResult>() {
                    @Override
                    public void onResponse(Call<DefaultResult> call, Response<DefaultResult> response) {
                        pd.dismiss();
                        Toast.makeText(MatkulUpdateActivity.this, "Data Berhasil di Update", Toast.LENGTH_LONG).show();
                    }

                    @Override
                    public void onFailure(Call<DefaultResult> call, Throwable t) {
                        pd.dismiss();
                        Toast.makeText(MatkulUpdateActivity.this, "Gagal di Update", Toast.LENGTH_LONG).show();
                    }
                });

                Intent intent = new Intent(MatkulUpdateActivity.this, MainMhsActivity.class);
                startActivity(intent);
            }
        });
    }
}