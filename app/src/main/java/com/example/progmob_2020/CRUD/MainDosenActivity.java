package com.example.progmob_2020.CRUD;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.example.progmob_2020.R;

public class MainDosenActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_dosen);

        Button btnGetDosen = (Button) findViewById(R.id.btnGetDosen);
        Button btnAddDosen = (Button) findViewById(R.id.btnAddDosen);
        Button btnDeleteDosen = (Button) findViewById(R.id.btnDeleteDosen);
        Button btnUpdateDosen = (Button) findViewById(R.id.btnUpdateDosen);

        btnGetDosen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainDosenActivity.this, DosenGetAllActivity.class);
                startActivity(intent);
            }
        });
        btnAddDosen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainDosenActivity.this, DosenAddActivity.class);
                startActivity(intent);
            }
        });
        btnDeleteDosen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainDosenActivity.this, DosenDeleteActivity.class);
                startActivity(intent);
            }
        });
        btnUpdateDosen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainDosenActivity.this, DosenUpdateActivity.class);
                startActivity(intent);
            }
        });
    }
}
