package com.example.progmob_2020.Pertemuan2;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;

import com.example.progmob_2020.Adapter.MahasiswaCardAdapter;
import com.example.progmob_2020.Model.Mahasiswa;
import com.example.progmob_2020.R;

import java.util.ArrayList;
import java.util.List;

public class CardViewTestActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_card_view_test);


                RecyclerView rv = (RecyclerView)findViewById(R.id.rvLatihan); //deklarasi dulu
                MahasiswaCardAdapter mahasiswaCardAdapter;

                //data
                List<Mahasiswa> mahasiswaList = new ArrayList<Mahasiswa>();

                // generate data mahasiswa
                Mahasiswa m1 = new Mahasiswa("Argo","72180101","082277401641");
                Mahasiswa m2 = new Mahasiswa("Halim","72180101","082277401641");
                Mahasiswa m3 = new Mahasiswa("Katon","72180101","082277401641");
                Mahasiswa m4 = new Mahasiswa("Siang","72180101","082277401641");
                Mahasiswa m5 = new Mahasiswa("Yetli","72180101","082277401641");

                mahasiswaList.add(m1);
                mahasiswaList.add(m2);
                mahasiswaList.add(m3);
                mahasiswaList.add(m4);
                mahasiswaList.add(m5);

                mahasiswaCardAdapter = new MahasiswaCardAdapter(CardViewTestActivity.this);
                mahasiswaCardAdapter.setMahasiswaList(mahasiswaList); //utk memasukan list ke adapter

                rv.setLayoutManager(new LinearLayoutManager(CardViewTestActivity.this));
                rv.setAdapter(mahasiswaCardAdapter);


            }
        }
